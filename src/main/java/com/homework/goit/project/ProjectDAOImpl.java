package com.homework.goit.project;

import com.homework.goit.common.DatabaseConnector;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalTime;
import java.util.*;

public class ProjectDAOImpl implements ProjectDAO {
    private static final Logger LOG = LogManager.getFormatterLogger(ProjectDAOImpl.class);
    private HikariDataSource dataSource = DatabaseConnector.getConnection();
    private final String INSERT = "INSERT INTO projects(name, release_date, cost, project_start) VALUES(?, ?, ?, ?);";
    private final String UPDATE = "UPDATE projects SET name = ?, release_date = ?, cost = ?, project_start = ? " +
            "WHERE id = ?;";
    private final String RETRIVE_BY_ID = "SELECT * FROM projects WHERE id = ?;";
    private final String RETRiVE_ALL = "SELECT * FROM projects;";
    private final String PROJECT_BY_SALARY = "SELECT pr.name, sum(dev.salary) FROM developers dev " +
            "JOIN  developers_projects dev_p ON dev.id = dev_p.developer_id " +
            "JOIN projects pr ON pr.id = dev_p.project_id " +
            "WHERE pr.id = ?  GROUP BY pr.name;";
    private final String GET_PROJECTS = "SELECT pr.project_start, pr.name, count(d.id) FROM projects pr " +
            "JOIN developers_projects dp on pr.id = dp.project_id " +
            "JOIN developers d on dp.developer_id = d.id " +
            "GROUP BY pr.name, pr.project_start;";

    @Override
    public void create(Project project) {
        LOG.info(String.format("create: project='%s'", project.toString()));
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT)) {
            statement.setString(1, project.getName());
            statement.setTimestamp(2, Timestamp.valueOf(project.getReleaseDate().atTime(LocalTime.MIDNIGHT)));
            statement.setInt(3, project.getCost());
            statement.setTimestamp(4, Timestamp.valueOf(project.getProjectStart().atTime(LocalTime.MIDNIGHT)));
            statement.execute();
        } catch (SQLException e) {
            LOG.error(String.format("create(project):error with '%s'", project.toString()), e);
        }
    }

    @Override
    public void update(Project project) {
        LOG.info(String.format("update: project='%s'", project.toString()));
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            statement.setString(1, project.getName());
            statement.setTimestamp(2, Timestamp.valueOf(project.getReleaseDate().atTime(LocalTime.MIDNIGHT)));
            statement.setInt(3, project.getCost());
            statement.setTimestamp(4, Timestamp.valueOf(project.getProjectStart().atTime(LocalTime.MIDNIGHT)));
            statement.setInt(5, project.getId());
            statement.execute();
            int rows = statement.executeUpdate();
            if (rows == 0) {
                LOG.warn(String.format("No rows updated, project with '%d' not found!'", project.getId()));
                throw new RuntimeException("No project with id = " + project.getId());
            }
        } catch (SQLException e) {
            LOG.error(String.format("update(project):error with '%s'", project.toString()), e);
        }
    }

    @Override
    public Project get(int id) {
        LOG.info(String.format("get: id='%d'", id));
        Project project = null;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(RETRIVE_BY_ID)) {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            rs.next();
            project = constructProject(rs);
        } catch (SQLException e) {
            LOG.error(String.format("get(id):error with id '%s'", id), e);
        }
        return project;
    }

    @Override
    public List<Project> getAll() {
        List<Project> projects = new LinkedList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(RETRiVE_ALL)) {
            ResultSet rs = statement.executeQuery();
            Project project;
            while (rs.next()) {
                project = constructProject(rs);
                projects.add(project);
            }
        } catch (SQLException e) {
            LOG.error("getAll:error -> ", e);
        }
        return projects;
    }

    @Override
    public void delete(int id) {
        LOG.info(String.format("delete: id='%d'", id));
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            String deleteLinkToCustomers = String.format("DELETE FROM customers_projects WHERE project_id = %d;", id);
            String deleteLinkToCompanies = String.format("DELETE FROM companies_projects WHERE project_id = %d;", id);
            String deleteLinkToDevelopers = String.format("DELETE FROM developers_projects WHERE project_id = %d;", id);
            String deleteProject = String.format("DELETE FROM projects WHERE id = %d;", id);
            statement.addBatch(deleteLinkToCustomers);
            statement.addBatch(deleteLinkToCompanies);
            statement.addBatch(deleteLinkToDevelopers);
            statement.addBatch(deleteProject);
            int[] rows = statement.executeBatch();
            if(rows[3] == 0){
                LOG.warn(String.format("No rows updated, project with '%d' not found!'", id));
                throw new RuntimeException("Project with id = " + id + " not found!");
            } else {
                LOG.debug(String.format("customers_projects affected rows=%d", rows[0]));
                LOG.debug(String.format("companies_projects affected rows=%d", rows[1]));
                LOG.debug(String.format("developers_projects affected rows=%d", rows[2]));
                LOG.debug(String.format("projects affected rows=%d", rows[3]));
            }
        } catch (SQLException e) {
            LOG.error(String.format("delete(id):error with id '%s'", id), e);
        }
    }

    @Override
    public Map<String, Integer> getSalaryByProject(int id) {
        Map<String, Integer> result = new HashMap<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(PROJECT_BY_SALARY)) {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String key = rs.getString(1);
                int value = rs.getInt(2);
                result.put(key, value);
            }
        } catch (SQLException e) {
            LOG.error(String.format("getSalaryByProject(id):error with id '%s'", id), e);
        }
        return result;
    }

    @Override
    public List<ProjectsInfo> getProjects() {
        List<ProjectsInfo> projectsInfo = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_PROJECTS)) {
            ResultSet rs = statement.executeQuery();
            ProjectsInfo projInfo;
            while (rs.next()) {
                projInfo = new ProjectsInfo();
                projInfo.setStartDate(rs.getTimestamp(1).toLocalDateTime().toLocalDate());
                projInfo.setProjectName(rs.getString(2));
                projInfo.setDevelopersInvolved(rs.getInt(3));
                projectsInfo.add(projInfo);
            }
        } catch (SQLException e) {
            LOG.error("getProjects():error ", e);
        }
        return projectsInfo;
    }

    private Project constructProject(ResultSet rs) throws SQLException {
        Project project = new Project();
        project.setId(rs.getInt("id"));
        project.setName(rs.getString("name"));
        project.setReleaseDate(rs.getTimestamp("release_date").toLocalDateTime().toLocalDate());
        project.setCost(rs.getInt("cost"));
        project.setProjectStart(rs.getTimestamp("project_start").toLocalDateTime().toLocalDate());
        return project;
    }
}
