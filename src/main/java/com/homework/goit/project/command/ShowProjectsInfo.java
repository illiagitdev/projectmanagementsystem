package com.homework.goit.project.command;

import com.homework.goit.common.Command;
import com.homework.goit.common.View;
import com.homework.goit.project.ProjectDAOImpl;
import com.homework.goit.project.ProjectDAO;
import com.homework.goit.project.ProjectsInfo;

import java.util.List;

public class ShowProjectsInfo implements Command {
    private View view;
    private ProjectDAO projectDAO;

    public ShowProjectsInfo(View view) {
        this.view = view;
        projectDAO = new ProjectDAOImpl();
    }

    @Override
    public String command() {
        return "show_projects_info";
    }

    @Override
    public void execute() {
        view.write("Projects: start date / project name / developers in project");
        List<ProjectsInfo> projects = projectDAO.getProjects();
        for (ProjectsInfo proj: projects) {
            view.write(String.format("%s / %S / %d", proj.getStartDate(), proj.getProjectName(), proj.getDevelopersInvolved()));
        }
    }
}
