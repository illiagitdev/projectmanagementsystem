package com.homework.goit.project.command;

import com.homework.goit.common.*;
import com.homework.goit.project.Project;
import com.homework.goit.project.ProjectDAO;
import com.homework.goit.project.ProjectDAOImpl;

import java.time.LocalDate;

public class UpdateProjects implements Command {
    private View view;
    private ProjectDAO projectDAO;

    public UpdateProjects(View view) {
        this.view = view;
        projectDAO = new ProjectDAOImpl();
    }

    @Override
    public String command() {
        return "update_project";
    }

    @Override
    public void execute() {
        view.write("Enter project id");
        int id = Utilita.validateNumber(view.read());
        if (!validId(id)){
            return;
        }
        view.write("Update project name");
        String name = Utilita.validate(view.read());
        view.write("Update release date (format YYYY-MM-DD)");
        LocalDate releaseDate = Utilita.validateDate(view.read());
        view.write("Update project cost");
        int cost = Utilita.validateNumber(view.read());
        view.write("Update project start date (format YYYY-MM-DD)");
        LocalDate startDate = Utilita.validateDate(view.read());
        Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setReleaseDate(releaseDate);
        project.setCost(cost);
        project.setProjectStart(startDate);
        view.write("Updating project...");
        try {
            projectDAO.update(project);
        } catch (RuntimeException e){
            view.write(e.getMessage());
        }
    }

    private boolean validId(int id){
        return true;
    }
}
