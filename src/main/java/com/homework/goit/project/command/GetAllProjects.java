package com.homework.goit.project.command;

import com.homework.goit.common.Command;
import com.homework.goit.common.View;
import com.homework.goit.project.Project;
import com.homework.goit.project.ProjectDAO;
import com.homework.goit.project.ProjectDAOImpl;

import java.util.List;

public class GetAllProjects implements Command {
    private View view;
    private ProjectDAO projectDAO;

    public GetAllProjects(View view) {
        this.view = view;
        projectDAO = new ProjectDAOImpl();
    }

    @Override
    public String command() {
        return "get_all_projects";
    }

    @Override
    public void execute() {
        view.write("Retrieving all projects...");
        List<Project> projects = projectDAO.getAll();
        for (Project pr : projects) {
            view.write(pr.toString());
        }
    }
}
