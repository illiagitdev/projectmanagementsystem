package com.homework.goit.project.command;

import com.homework.goit.common.*;
import com.homework.goit.project.Project;
import com.homework.goit.project.ProjectDAO;
import com.homework.goit.project.ProjectDAOImpl;

public class GetProjectById implements Command {
    private View view;
    private ProjectDAO projectDAO;

    public GetProjectById(View view) {
        this.view = view;
        projectDAO = new ProjectDAOImpl();
    }

    @Override
    public String command() {
        return "get_project_by_id";
    }

    @Override
    public void execute() {
        view.write("Enter project id");
        int id = Utilita.validateNumber(view.read());
        view.write("Searching project...");
        Project project = projectDAO.get(id);
        if(project != null){
            view.write(project.toString());
        }else {
            view.write("Project with such id were not found!");
        }
    }
}
