package com.homework.goit.project.command;

import com.homework.goit.common.*;
import com.homework.goit.project.ProjectDAOImpl;
import com.homework.goit.project.ProjectDAO;

import java.util.Map;

public class TotalSalaryInProjects implements Command {
    private View view;
    private ProjectDAO projectDAO;

    public TotalSalaryInProjects(View view) {
        this.view = view;
        projectDAO = new ProjectDAOImpl();
    }

    @Override
    public String command() {
        return "total_salary_in_project";
    }

    @Override
    public void execute() {
        view.write("Enter project id to see total salary");
        int id = Utilita.validateNumber(view.read());
        view.write("Project id(" + id + "):");
        Map<String, Integer> projectSalary = projectDAO.getSalaryByProject(id);
        view.write(projectSalary.toString());
    }
}
