package com.homework.goit.project;

import com.homework.goit.common.Entity;

import java.time.LocalDate;

public class Project extends Entity {
    private String name;
    private LocalDate releaseDate;
    private int cost;
    private LocalDate  projectStart;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public LocalDate getProjectStart() {
        return projectStart;
    }

    public void setProjectStart(LocalDate projectStart) {
        this.projectStart = projectStart;
    }

    @Override
    public String toString() {
        return "Project{" +
                super.toString() + ' ' +
                ", name='" + name + '\'' +
                ", releaseDate=" + releaseDate +
                ", cost=" + cost +
                ", projectStart=" + projectStart +
                '}';
    }
}
