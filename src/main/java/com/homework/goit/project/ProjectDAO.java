package com.homework.goit.project;

import com.homework.goit.common.DataAccessObject;

import java.util.List;
import java.util.Map;

public interface ProjectDAO extends DataAccessObject<Project> {
    Map<String, Integer> getSalaryByProject(int id);
    List<ProjectsInfo> getProjects();
}
