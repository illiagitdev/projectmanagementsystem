package com.homework.goit.project;

import java.time.LocalDate;

public class ProjectsInfo {
    private LocalDate startDate;
    private String projectName;
    private int developersInvolved;

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public int getDevelopersInvolved() {
        return developersInvolved;
    }

    public void setDevelopersInvolved(int developersInvolved) {
        this.developersInvolved = developersInvolved;
    }
}
