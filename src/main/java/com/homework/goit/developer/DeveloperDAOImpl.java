package com.homework.goit.developer;

import com.homework.goit.common.DatabaseConnector;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.*;

public class DeveloperDAOImpl implements DeveloperDAO {
    private static final Logger LOG = LogManager.getFormatterLogger(DeveloperDAOImpl.class);
    private HikariDataSource dataSource = DatabaseConnector.getConnection();
    private final String INSERT = "INSERT INTO developers(first_name, last_name, age, email, sex, salary) " +
            "VALUES(?, ?, ?, ?, ?, ?);";
    private final String UPDATE = "UPDATE developers SET first_name = ?, last_name = ?, age = ?, email = ?, sex = ?, " +
            "salary = ? WHERE id = ?;";
    private final String RETRIVE_BY_ID = "SELECT * FROM developers WHERE id = ?;";
    private final String RETRiVE_ALL = "SELECT * FROM developers;";

    private final String GET_ALL_JAVA_DEVELOPERS = "SELECT dev.* FROM developers dev " +
            "JOIN developers_skills ds ON dev.id = ds.developer_id " +
            "JOIN skills s on ds.skill_id = s.id " +
            "WHERE s.skill = ?;";
    private final String GET_ALL_MIDDLE_DEVELOPERS = "SELECT s.skill, dev.* FROM developers dev " +
            "JOIN developers_skills ds ON dev.id = ds.developer_id " +
            "JOIN skills s on ds.skill_id = s.id " +
            "WHERE s.level = ? " +
            "GROUP BY dev.id, s.skill;";
    private final String DEVELOPERS_IN_PROJECT = "SELECT pr.name, dev.* FROM developers dev " +
            "JOIN  developers_projects dev_p ON dev.id = dev_p.developer_id " +
            "JOIN projects pr ON pr.id = dev_p.project_id " +
            "WHERE pr.id = ? " +
            "GROUP BY pr.name, dev.id " +
            "ORDER BY pr.name;";


    @Override
    public void create(Developer developer) {
        LOG.info(String.format("create: developer='%s'", developer.toString()));
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT)){
            statement.setString(1, developer.getFirstName());
            statement.setString(2, developer.getLastName());
            statement.setInt(3, developer.getAge());
            statement.setString(4, developer.getEmail());
            statement.setString(5, developer.getGender().getGender());
            statement.setInt(6, developer.getSalary());
            statement.execute();
        } catch (SQLException e) {
            LOG.error(String.format("create(developer):error with '%s'", developer.toString()), e);
        }
    }

    @Override
    public void update(Developer developer) {
        LOG.info(String.format("update: developer='%s'", developer.toString()));
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE)){
            statement.setString(1, developer.getFirstName());
            statement.setString(2, developer.getLastName());
            statement.setInt(3, developer.getAge());
            statement.setString(4, developer.getEmail());
            statement.setString(5, developer.getGender().getGender());
            statement.setInt(6, developer.getSalary());
            statement.setInt(7, developer.getId());
            int rows = statement.executeUpdate();
            if(rows == 0){
                LOG.warn(String.format("No rows updated, developer with '%d' not found!'", developer.getId()));
                throw new RuntimeException("No developer with id = " + developer.getId());
            }
        } catch (SQLException e) {
            LOG.error(String.format("update(developer):error with '%s'", developer.toString()), e);
        }
    }

    @Override
    public Developer get(int id) {
        LOG.info(String.format("get: id='%d'", id));
        Developer developer = null;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(RETRIVE_BY_ID)){
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            rs.next();
            developer = buildDeveloper(rs);
        } catch (SQLException e) {
            LOG.error(String.format("get(id):error with id '%s'", id), e);
        }
        return developer;
    }

    @Override
    public List<Developer> getAll() {
        List<Developer> developers = new LinkedList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(RETRiVE_ALL)){
            ResultSet rs = statement.executeQuery();
            Developer developer;
            while (rs.next()){
                developer = buildDeveloper(rs);
                developers.add(developer);
            }
        } catch (SQLException e) {
            LOG.error("getAll:error -> ", e);
        }
        return developers;
    }

    @Override
    public void delete(int id) {
        LOG.info(String.format("delete: id='%d'", id));
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            String deleteLinkToSkills = String.format("DELETE FROM developers_skills WHERE developer_id = %d;", id);
            String deleteLinkToCompanies = String.format("DELETE FROM developers_companies WHERE developer_id = %d;", id);
            String deleteLinkToProjects = String.format("DELETE FROM developers_projects WHERE developer_id = %d;", id);
            String deleteDeveloper = String.format("DELETE FROM developers WHERE id = %d;", id);
            statement.addBatch(deleteLinkToSkills);
            statement.addBatch(deleteLinkToCompanies);
            statement.addBatch(deleteLinkToProjects);
            statement.addBatch(deleteDeveloper);
            int[] rows = statement.executeBatch();
            if(rows[3] == 0){
                LOG.warn(String.format("No rows updated, developer with '%d' not found!'", id));
                throw new RuntimeException("Developer with id = " + id + " not found!");
            } else {
                LOG.debug(String.format("developers_skills affected rows=%d", rows[0]));
                LOG.debug(String.format("developers_companies affected rows=%d", rows[1]));
                LOG.debug(String.format("developers_projects affected rows=%d", rows[2]));
                LOG.debug(String.format("developers affected rows=%d", rows[3]));
            }
        } catch (SQLException e) {
            LOG.error(String.format("delete(id):error with id '%s'", id), e);
        }
    }

    @Override
    public List<Developer> getJavaDeveloper() {
        List<Developer> developers = new LinkedList<>();;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ALL_JAVA_DEVELOPERS)){
            statement.setString(1, "Java");
            ResultSet rs = statement.executeQuery();
            Developer developer;
            while (rs.next()){
                developer = buildDeveloper(rs);
                developers.add(developer);
            }
        } catch (SQLException e) {
            LOG.error("getJavaDeveloper():error -> '", e);
        }
        return developers;
    }

    @Override
    public Map<String, Developer> getMiddleDevelopers() {
        Map<String, Developer> developers = new HashMap<>();;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ALL_MIDDLE_DEVELOPERS)){
            statement.setString(1, "Middle");
            ResultSet rs = statement.executeQuery();
            Developer developer;
            String skill;
            int n = 1;
            while (rs.next()){
                skill = rs.getString(1);
                developer = buildDeveloper(rs);
                developers.put(String.format("%s (%d) ", skill , n++), developer);
            }
        } catch (SQLException e) {
            LOG.error("getMiddleDevelopers():error -> '", e);
        }
        return developers;
    }

    @Override
    public Map<String, Developer> getDevelopersInProject(int id) {
        LOG.info(String.format("getDevelopersInProject(id): id='%d'", id));
        Map<String, Developer> developers = new HashMap<>();;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(DEVELOPERS_IN_PROJECT)){
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            Developer developer;
            String project;
            int n = 1;
            while (rs.next()){
                project = rs.getString(1);
                developer = buildDeveloper(rs);
                developers.put(String.format("%s (%d) ", project , n++), developer);
            }
        } catch (SQLException e) {
            LOG.error(String.format("getDevelopersInProject(id):error with id '%s'", id), e);
        }
        return  developers;
    }

    private Developer buildDeveloper(ResultSet rs) throws SQLException {
        Developer developer = new Developer();
        developer.setId(rs.getInt("id"));
        developer.setFirstName(rs.getString("first_name"));
        developer.setLastName(rs.getString("last_name"));
        developer.setAge(rs.getInt("age"));
        developer.setEmail(rs.getString("email"));
        Optional<Gender> tmp = Gender.getGenderValue(rs.getString("sex").toLowerCase());
        Gender gender = tmp.isEmpty() ? Gender.OTHER : tmp.get();
        developer.setGender(gender);
        developer.setSalary(rs.getInt("salary"));
        return developer;
    }
}
