package com.homework.goit.developer;

import com.homework.goit.common.DataAccessObject;

import java.util.List;
import java.util.Map;

public interface DeveloperDAO extends DataAccessObject<Developer> {

    List<Developer> getJavaDeveloper();
    Map<String, Developer> getMiddleDevelopers();
    Map<String, Developer> getDevelopersInProject(int id);

}
