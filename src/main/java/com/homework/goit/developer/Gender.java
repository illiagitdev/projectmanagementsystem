package com.homework.goit.developer;

import java.util.Arrays;
import java.util.Optional;

public enum Gender {
    MALE("male"),
    FEMALE("female"),
    OTHER("other");

    private String gender;

    Gender(String gender) {
        this.gender = gender;
    }

    String getGender(){
        return gender;
    }

    public static Optional<Gender> getGenderValue(String value) {
        return Arrays.stream(Gender.values())
                .filter(enumValue -> enumValue.getGender().equals(value))
                .findAny();
    }
}
