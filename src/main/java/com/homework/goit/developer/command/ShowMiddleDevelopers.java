package com.homework.goit.developer.command;

import com.homework.goit.common.Command;
import com.homework.goit.common.View;
import com.homework.goit.developer.Developer;
import com.homework.goit.developer.DeveloperDAO;
import com.homework.goit.developer.DeveloperDAOImpl;

import java.util.Iterator;
import java.util.Map;

public class ShowMiddleDevelopers implements Command {
    private View view;
    private DeveloperDAO developerDAO;

    public ShowMiddleDevelopers(View view) {
        this.view = view;
        developerDAO = new DeveloperDAOImpl();
    }

    @Override
    public String command() {
        return "show_middle_developers";
    }

    @Override
    public void execute() {
        view.write("All Middle developers by skill.");
        Map<String, Developer> developers = developerDAO.getMiddleDevelopers();
        Iterator<Map.Entry<String, Developer>> iter = developers.entrySet().iterator();
        while (iter.hasNext()){
            Map.Entry<String, Developer> entry = iter.next();
            view.write(entry.getKey() + ": " + entry.getValue());
        }
    }
}
