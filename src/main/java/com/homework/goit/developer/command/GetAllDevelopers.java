package com.homework.goit.developer.command;

import com.homework.goit.common.Command;
import com.homework.goit.common.View;
import com.homework.goit.developer.Developer;
import com.homework.goit.developer.DeveloperDAO;
import com.homework.goit.developer.DeveloperDAOImpl;

import java.util.List;

public class GetAllDevelopers implements Command {
    private View view;
    private DeveloperDAO developerDAO;

    public GetAllDevelopers(View view) {
        this.view = view;
        developerDAO = new DeveloperDAOImpl();
    }

    @Override
    public String command() {
        return "get_all_developers";
    }

    @Override
    public void execute() {
        view.write("Retrieving all developers...");
        List<Developer> developers = developerDAO.getAll();
        for (Developer dev : developers) {
            view.write(dev.toString());
        }
    }
}
