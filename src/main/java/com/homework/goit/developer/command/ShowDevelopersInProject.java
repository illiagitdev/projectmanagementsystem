package com.homework.goit.developer.command;

import com.homework.goit.common.*;
import com.homework.goit.developer.Developer;
import com.homework.goit.developer.DeveloperDAO;
import com.homework.goit.developer.DeveloperDAOImpl;

import java.util.Iterator;
import java.util.Map;

public class ShowDevelopersInProject implements Command {
    private View view;
    private DeveloperDAO developerDAO;

    public ShowDevelopersInProject(View view) {
        this.view = view;
        developerDAO = new DeveloperDAOImpl();
    }

    @Override
    public String command() {
        return "show_developers_in_project";
    }

    @Override
    public void execute() {
        view.write("Enter project id to see it's developers");
        int id = Utilita.validateNumber(view.read());
        Map<String, Developer> projectDevelopers = developerDAO.getDevelopersInProject(id);
        Iterator<Map.Entry<String, Developer>> iter = projectDevelopers.entrySet().iterator();
        while (iter.hasNext()){
            Map.Entry<String, Developer> entry = iter.next();
            view.write(entry.getKey() + ": " + entry.getValue());
        }
    }
}
