package com.homework.goit.developer.command;

import com.homework.goit.common.*;
import com.homework.goit.developer.Developer;
import com.homework.goit.developer.DeveloperDAO;
import com.homework.goit.developer.DeveloperDAOImpl;
import com.homework.goit.developer.Gender;

import java.util.Optional;

public class CreateDeveloper implements Command {
    private View view;
    private DeveloperDAO developerDAO;

    public CreateDeveloper(View view) {
        this.view = view;
        developerDAO = new DeveloperDAOImpl();
    }

    @Override
    public String command() {
        return "create_developer";
    }

    @Override
    public void execute() {
        view.write("Enter developer first name");
        String firstName = Utilita.validate(view.read());
        view.write("Enter developer last name");
        String lastName = Utilita.validate(view.read());
        view.write("Enter developer age (>0)");
        int age = Utilita.validateNumber(view.read());
        view.write("Enter developer email");
        String email = Utilita.validate(view.read());
        view.write("Enter developer sex(male, female)");
        String sex = Utilita.validate(view.read());
        view.write("Enter developer salary");
        int salary = Utilita.validateNumber(view.read());
        Developer developer = new Developer();
        developer.setFirstName(firstName);
        developer.setLastName(lastName);
        developer.setAge(age);
        developer.setEmail(email);
        Optional<Gender> tmp = Gender.getGenderValue(sex.toLowerCase());
        Gender gender = tmp.isEmpty() ? Gender.OTHER : tmp.get();
        developer.setGender(gender);
        developer.setSalary(salary);
        view.write("Creating developer...");
        developerDAO.create(developer);
    }
}
