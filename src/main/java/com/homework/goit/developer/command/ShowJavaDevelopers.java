package com.homework.goit.developer.command;

import com.homework.goit.common.Command;
import com.homework.goit.common.View;
import com.homework.goit.developer.Developer;
import com.homework.goit.developer.DeveloperDAO;
import com.homework.goit.developer.DeveloperDAOImpl;

import java.util.List;

public class ShowJavaDevelopers implements Command {
    private View view;
    private DeveloperDAO developerDAO;

    public ShowJavaDevelopers(View view) {
        this.view = view;
        developerDAO = new DeveloperDAOImpl();
    }

    @Override
    public String command() {
        return "show_java_developers";
    }

    @Override
    public void execute() {
        view.write("Java developers:");
        List<Developer> developers = developerDAO.getJavaDeveloper();
        for (Developer d: developers) {
            view.write(d.toString());
        }
    }
}
