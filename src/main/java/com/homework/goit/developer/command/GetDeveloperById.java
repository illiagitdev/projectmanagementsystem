package com.homework.goit.developer.command;

import com.homework.goit.common.*;
import com.homework.goit.developer.Developer;
import com.homework.goit.developer.DeveloperDAO;
import com.homework.goit.developer.DeveloperDAOImpl;

public class GetDeveloperById implements Command {
    private View view;
    private DeveloperDAO developerDAO;

    public GetDeveloperById(View view) {
        this.view = view;
        developerDAO = new DeveloperDAOImpl();
    }

    @Override
    public String command() {
        return "get_developer_by_id";
    }

    @Override
    public void execute() {
        view.write("Enter id developer to search");
        int id = Utilita.validateNumber(view.read());
        view.write("Searching for developer...");
        Developer developer = developerDAO.get(id);
        if (developer != null) {
            view.write(developer.toString());
        } else {
            view.write("Developer with such id were not found!");
        }
    }
}
