package com.homework.goit.developer.command;

import com.homework.goit.common.*;
import com.homework.goit.developer.DeveloperDAO;
import com.homework.goit.developer.DeveloperDAOImpl;

public class DeleteDeveloper implements Command {
    private View view;
    private DeveloperDAO developerDAO;

    public DeleteDeveloper(View view) {
        this.view = view;
        developerDAO = new DeveloperDAOImpl();
    }

    @Override
    public String command() {
        return "delete_developer";
    }

    @Override
    public void execute() {
        view.write("Enter id developer to delete");
        int id = Utilita.validateNumber(view.read());
        view.write("Deleting developer...");
        try {
            developerDAO.delete(id);
        } catch (RuntimeException e){
            view.write(e.getMessage());
        }
    }
}
