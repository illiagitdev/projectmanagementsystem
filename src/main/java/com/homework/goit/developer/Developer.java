package com.homework.goit.developer;

import com.homework.goit.common.Entity;

public class Developer extends Entity {
    private String firstName;
    private String lastName;
    private int age;
    private String email;
    private Gender gender;
    private int salary;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        if (salary < 0){
            this.salary = 0;
        }
        else {
            this.salary = salary;
        }
    }

    @Override
    public String toString() {
        return "Developer{" +
                super.toString() + ' ' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                ", sex='" + gender.getGender() + '\'' +
                ", salary=" + salary +
                '}';
    }
}
