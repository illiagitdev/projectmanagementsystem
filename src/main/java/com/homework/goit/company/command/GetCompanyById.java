package com.homework.goit.company.command;

import com.homework.goit.common.*;
import com.homework.goit.company.Company;
import com.homework.goit.company.CompanyDAOImpl;

public class GetCompanyById implements Command {
    private View view;
    private DataAccessObject<Company> companyDAO;

    public GetCompanyById(View view) {
        this.view = view;
        companyDAO = new CompanyDAOImpl();
    }

    @Override
    public String command() {
        return "get_company_by_id";
    }

    @Override
    public void execute() {
        view.write("Enter company id");
        int id = Utilita.validateNumber(view.read());
        view.write("Searching company...");
        Company company = companyDAO.get(id);
        if (company != null) {
            view.write(company.toString());
        } else {
            view.write("Company with such id were not found!");
        }
    }
}
