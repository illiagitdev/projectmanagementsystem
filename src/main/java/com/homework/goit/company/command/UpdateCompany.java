package com.homework.goit.company.command;

import com.homework.goit.common.*;
import com.homework.goit.company.Company;
import com.homework.goit.company.CompanyDAOImpl;

public class UpdateCompany implements Command {
    private View view;
    private DataAccessObject<Company> companyDAO;

    public UpdateCompany(View view) {
        this.view = view;
        companyDAO = new CompanyDAOImpl();
    }

    @Override
    public String command() {
        return "update_company";
    }

    @Override
    public void execute() {
        view.write("Enter company id for update");
        int id = Utilita.validateNumber(view.read());
        view.write("Update company name");
        String name = Utilita.validate(view.read());
        view.write("Update company location");
        String location = view.read();
        Company company = new Company();
        company.setId(id);
        company.setName(name);
        company.setLocation(location);
        view.write("Updating company...");
        try {
            companyDAO.update(company);
        } catch (RuntimeException e){
            view.write(e.getMessage());
        }
    }
}
