package com.homework.goit.company.command;

import com.homework.goit.common.Command;
import com.homework.goit.common.DataAccessObject;
import com.homework.goit.common.View;
import com.homework.goit.company.Company;
import com.homework.goit.company.CompanyDAOImpl;

import java.util.List;

public class GetAllCompanies implements Command {
    private View view;
    private DataAccessObject<Company> companyDAO;

    public GetAllCompanies(View view) {
        this.view = view;
        companyDAO = new CompanyDAOImpl();
    }

    @Override
    public String command() {
        return "get_all_companies";
    }

    @Override
    public void execute() {
        view.write("Retrieving all companies...");
        List<Company> companies = companyDAO.getAll();
        for (Company com: companies) {
            view.write(com.toString());
        }
    }
}
