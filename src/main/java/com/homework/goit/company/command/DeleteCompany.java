package com.homework.goit.company.command;

import com.homework.goit.common.*;
import com.homework.goit.company.Company;
import com.homework.goit.company.CompanyDAOImpl;

public class DeleteCompany implements Command {
    private View view;
    private DataAccessObject<Company> companyDAO;

    public DeleteCompany(View view) {
        this.view = view;
        companyDAO = new CompanyDAOImpl();
    }

    @Override
    public String command() {
        return "delete_company";
    }

    @Override
    public void execute() {
        view.write("Enter company id for deleting");
        int id = Utilita.validateNumber(view.read());
        view.write("Deleting companies...");
        try {
            companyDAO.delete(id);
        } catch (RuntimeException e){
            view.write(e.getMessage());
        }
    }
}
