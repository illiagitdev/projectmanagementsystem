package com.homework.goit.company.command;

import com.homework.goit.common.*;
import com.homework.goit.company.Company;
import com.homework.goit.company.CompanyDAOImpl;

public class CreateCompany implements Command {
    private View view;
    private DataAccessObject<Company> companyDAO;

    public CreateCompany(View view) {
        this.view = view;
        companyDAO = new CompanyDAOImpl();
    }

    @Override
    public String command() {
        return "create_company";
    }

    @Override
    public void execute() {
        view.write("Enter company name");
        String name = Utilita.validate(view.read());
        view.write("Enter company location");
        String location = view.read();
        Company company = new Company();
        company.setName(name);
        company.setLocation(location);
        view.write("Creating new company...");
        companyDAO.create(company);
    }
}
