package com.homework.goit.company;

import com.homework.goit.common.DataAccessObject;
import com.homework.goit.common.DatabaseConnector;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class CompanyDAOImpl implements DataAccessObject<Company> {
    private static final Logger LOG = LogManager.getFormatterLogger(CompanyDAOImpl.class);
    private HikariDataSource dataSource = DatabaseConnector.getConnection();
    private final String INSERT = "INSERT INTO companies(name, location) VALUES(?, ?);";
    private final String UPDATE = "UPDATE companies SET name = ?, location = ? WHERE id = ?;";
    private final String RETRIVE_BY_ID = "SELECT * FROM companies WHERE id = ?;";
    private final String RETRiVE_ALL = "SELECT * FROM companies;";

    @Override
    public void create(Company company) {
        LOG.info(String.format("create: company='%s'", company.toString()));
        try (Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(INSERT)) {
            statement.setString(1, company.getName());
            statement.setString(2, company.getLocation());
            statement.execute();
        } catch (SQLException e) {
            LOG.error(String.format("create(company):error with '%s'", company.toString()), e);
        }
    }

    @Override
    public void update(Company company) {
        LOG.info(String.format("update: company='%s'", company.toString()));
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            statement.setString(1, company.getName());
            statement.setString(2, company.getLocation());
            statement.setInt(3, company.getId());
            int rows = statement.executeUpdate();
            if (rows == 0) {
                LOG.warn(String.format("No rows updated, company with '%d' not found!'", company.getId()));
                throw new RuntimeException("No company with id = " + company.getId());
            }
        } catch (SQLException e) {
            LOG.error(String.format("update(company):error with '%s'", company.toString()), e);
        }
    }

    @Override
    public Company get(int id) {
        LOG.info(String.format("get: id='%d'", id));
        Company company = null;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(RETRIVE_BY_ID)) {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            rs.next();
            company = buildCompany(rs);
        } catch (SQLException e) {
            LOG.error(String.format("get(id):error with id '%s'", id), e);
        }
        return company;
    }

    @Override
    public List<Company> getAll() {
        List<Company> companyList = new LinkedList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(RETRiVE_ALL)) {
            ResultSet rs = statement.executeQuery();
            Company company;
            while (rs.next()) {
                company = buildCompany(rs);
                companyList.add(company);
            }
        } catch (SQLException e) {
            LOG.error("getAll:error -> ", e);
        }
        return companyList;
    }

    @Override
    public void delete(int id) {
        LOG.info(String.format("delete: id='%d'", id));
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            String deleteLinkToProjects = String.format("DELETE FROM companies_projects WHERE company_id = %d;", id);
            String deleteLinkToDevelopers = String.format("DELETE FROM developers_companies WHERE company_id = %d;", id);
            String deleteCompany = String.format("DELETE FROM companies WHERE id = %d;", id);
            statement.addBatch(deleteLinkToProjects);
            statement.addBatch(deleteLinkToDevelopers);
            statement.addBatch(deleteCompany);
            int[] rows = statement.executeBatch();
            if (rows[2] == 0) {
                LOG.warn(String.format("No rows updated, company with '%d' not found!'", id));
                throw new RuntimeException("Company with id = " + id + " were not found!");
            } else {
                LOG.debug(String.format("companies_projects affected rows=%d", rows[0]));
                LOG.debug(String.format("developers_companies affected rows=%d", rows[1]));
                LOG.debug(String.format("companies affected rows=%d", rows[2]));
            }
        } catch (SQLException e) {
            LOG.error(String.format("delete(id):error with id '%s'", id), e);
        }
    }

    private Company buildCompany(ResultSet rs) throws SQLException {
        Company company = new Company();
        company.setId(rs.getInt("id"));
        company.setName(rs.getString("name"));
        company.setLocation(rs.getString("location"));
        return company;
    }
}
