package com.homework.goit.customer;

import com.homework.goit.common.DataAccessObject;
import com.homework.goit.common.DatabaseConnector;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class CustomerDAOImpl implements DataAccessObject<Customer> {
    private static final Logger LOG = LogManager.getFormatterLogger(CustomerDAOImpl.class);
    private HikariDataSource dataSource = DatabaseConnector.getConnection();
    private final String INSERT = "INSERT INTO customers(name, budget) VALUES(?, ?);";
    private final String UPDATE = "UPDATE customers SET name = ?, budget = ? WHERE id = ?;";
    private final String RETRIVE_BY_ID = "SELECT * FROM customers WHERE id = ?;";
    private final String RETRiVE_ALL = "SELECT * FROM customers;";

    @Override
    public void create(Customer customer) {
        LOG.info(String.format("create: customer='%s'", customer.toString()));
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT)){
            statement.setString(1, customer.getName());
            statement.setInt(2, customer.getBudget());
            statement.execute();
        } catch (SQLException e) {
            LOG.error(String.format("create(customer):error with '%s'", customer.toString()), e);
        }
    }

    @Override
    public void update(Customer customer) {
        LOG.info(String.format("update: customer='%s'", customer.toString()));
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE)){
            statement.setString(1, customer.getName());
            statement.setInt(2, customer.getBudget());
            statement.setInt(3, customer.getId());
            int rows = statement.executeUpdate();
            if(rows == 0){
                LOG.warn(String.format("No rows updated, customer with '%d' not found!'", customer.getId()));
                throw new RuntimeException("No customer with id = " + customer.getId());
            }
        } catch (SQLException e) {
            LOG.error(String.format("update(customer):error with '%s'", customer.toString()), e);
        }
    }

    @Override
    public Customer get(int id) {
        LOG.info(String.format("get: id='%d'", id));
        Customer customer = null;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(RETRIVE_BY_ID)){
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            rs.next();
            customer = constructCustomer(rs);
        } catch (SQLException e) {
            LOG.error(String.format("get(id):error with id '%s'", id), e);
        }
        return customer;
    }

    @Override
    public List<Customer> getAll() {
        List<Customer> customerList = new LinkedList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(RETRiVE_ALL)){
            ResultSet rs = statement.executeQuery();
            Customer customer;
            while (rs.next()){
                customer = constructCustomer(rs);
                customerList.add(customer);
            }
        } catch (SQLException e) {
            LOG.error("getAll:error -> ", e);
        }
        return customerList;
    }

    @Override
    public void delete(int id) {
        LOG.info(String.format("delete: id='%d'", id));
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()){
            String deleteLinkToProjects = String.format("DELETE FROM customers_projects WHERE customer_id = %d;", id);
            String deleteCustomer = String.format("DELETE FROM customers WHERE id = %d;", id);
            statement.addBatch(deleteLinkToProjects);
            statement.addBatch(deleteCustomer);
            int[] rows = statement.executeBatch();
            if(rows[1] == 0){
                LOG.warn(String.format("No rows updated, customer with '%d' not found!'", id));
                throw new RuntimeException("Customer with id = " + id + " not found!");
            } else {
                LOG.debug(String.format("customers_projects affected rows=%d", rows[0]));
                LOG.debug(String.format("customers affected rows=%d", rows[1]));
            }
        } catch (SQLException e) {
            LOG.error(String.format("delete(id):error with id '%s'", id), e);
        }
    }

    private Customer constructCustomer(ResultSet rs) throws SQLException {
        Customer customer = new Customer();
        customer.setId(rs.getInt("id"));
        customer.setName(rs.getString("name"));
        customer.setBudget(rs.getInt("budget"));
        return customer;
    }
}
