package com.homework.goit.customer.command;

import com.homework.goit.common.*;
import com.homework.goit.customer.Customer;
import com.homework.goit.customer.CustomerDAOImpl;

public class UpdateCustomer implements Command {
    private View view;
    private DataAccessObject<Customer> customerDAO;

    public UpdateCustomer(View view) {
        this.view = view;
        customerDAO = new CustomerDAOImpl();
    }

    @Override
    public String command() {
        return "update_customer";
    }

    @Override
    public void execute() {
        view.write("Enter customer id for update");
        int id = Utilita.validateNumber(view.read());
        view.write("Update customer name");
        String name = Utilita.validate(view.read());
        view.write("Update customer budget");
        int budget = Utilita.validateNumber(view.read());
        Customer customer = new Customer();
        customer.setId(id);
        customer.setName(name);
        customer.setBudget(budget);
        view.write("Updating customer...");
        try {
            customerDAO.update(customer);
        }catch (RuntimeException e){
            view.write(e.getMessage());
        }
    }
}
