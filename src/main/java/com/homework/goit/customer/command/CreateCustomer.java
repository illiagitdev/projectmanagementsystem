package com.homework.goit.customer.command;

import com.homework.goit.common.*;
import com.homework.goit.customer.Customer;
import com.homework.goit.customer.CustomerDAOImpl;

public class CreateCustomer implements Command {
    private View view;
    private DataAccessObject<Customer> customerDAO;

    public CreateCustomer(View view) {
        this.view = view;
        customerDAO = new CustomerDAOImpl();
    }

    @Override
    public String command() {
        return "create_customer";
    }

    @Override
    public void execute() {
        view.write("Enter customer name");
        String name = Utilita.validate(view.read());
        view.write("Enter customer budget");
        int budget = Utilita.validateNumber(view.read());
        view.write("Creating customer...");
        Customer customer = new Customer();
        customer.setName(name);
        customer.setBudget(budget);
        customerDAO.create(customer);
    }
}
