package com.homework.goit.customer.command;

import com.homework.goit.common.*;
import com.homework.goit.customer.Customer;
import com.homework.goit.customer.CustomerDAOImpl;

public class DeleteCustomer implements Command {
    private View view;
    private DataAccessObject<Customer> customerDAO;

    public DeleteCustomer(View view) {
        this.view = view;
        customerDAO = new CustomerDAOImpl();
    }

    @Override
    public String command() {
        return "delete_customer";
    }

    @Override
    public void execute() {
        view.write("Enter customer id to delete");
        int id = Utilita.validateNumber(view.read());
        view.write("Deleting customer...");
        try {
            customerDAO.delete(id);
        } catch (RuntimeException e){
            view.write(e.getMessage());
        }
    }
}
