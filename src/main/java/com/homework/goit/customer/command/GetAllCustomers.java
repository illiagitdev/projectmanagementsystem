package com.homework.goit.customer.command;

import com.homework.goit.common.Command;
import com.homework.goit.common.DataAccessObject;
import com.homework.goit.common.View;
import com.homework.goit.customer.Customer;
import com.homework.goit.customer.CustomerDAOImpl;

import java.util.List;

public class GetAllCustomers implements Command {
    private View view;
    private DataAccessObject<Customer> customerDAO;

    public GetAllCustomers(View view) {
        this.view = view;
        customerDAO = new CustomerDAOImpl();
    }

    @Override
    public String command() {
        return "get_all_customers";
    }

    @Override
    public void execute() {
        view.write("Retrieving all customers...");
        List<Customer> customers = customerDAO.getAll();
        for (Customer cs: customers) {
            view.write(cs.toString());
        }
    }
}
