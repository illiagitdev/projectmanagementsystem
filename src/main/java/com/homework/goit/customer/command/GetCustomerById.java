package com.homework.goit.customer.command;

import com.homework.goit.common.*;
import com.homework.goit.customer.Customer;
import com.homework.goit.customer.CustomerDAOImpl;

public class GetCustomerById implements Command {
    private View view;
    private DataAccessObject<Customer> customerDAO;

    public GetCustomerById(View view) {
        this.view = view;
        customerDAO = new CustomerDAOImpl();
    }

    @Override
    public String command() {
        return "get_customer_by_id";
    }

    @Override
    public void execute() {
        view.write("Enter customer id");
        int id = Utilita.validateNumber(view.read());
        view.write("Searching customer...");
        Customer customer = customerDAO.get(id);
        if (customer != null) {
            view.write(customer.toString());
        } else {
            view.write("Customer with such id were not found!");
        }
    }
}
