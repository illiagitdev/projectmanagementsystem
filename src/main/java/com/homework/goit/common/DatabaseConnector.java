package com.homework.goit.common;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.util.Properties;

public class DatabaseConnector {
    private static final Logger LOG = LogManager.getLogger(DatabaseConnector.class);
    private static final HikariDataSource ds;

    private DatabaseConnector() {
        throw new RuntimeException("This operation not supported!");
    }

    static {
        HikariConfig config = new HikariConfig();
        Properties properties = new Properties();
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try (InputStream propertyStream = classLoader.getResourceAsStream("application.properties")){
            properties.load(propertyStream);
        } catch (Exception e) {
            LOG.error("error loading application properties", e);
            throw new RuntimeException("Error loading application.properties", e);
        }
        config.setJdbcUrl(properties.getProperty("jdbc.url"));
        config.setUsername(properties.getProperty("jdbc.username"));
        config.setPassword(properties.getProperty("jdbc.password"));
        ds = new HikariDataSource(config);
        ds.setMaximumPoolSize(Integer.parseInt(properties.getProperty("hikari.connection.max.pool.size")));
    }

    public static HikariDataSource getConnection(){
        return ds;
    }
}
