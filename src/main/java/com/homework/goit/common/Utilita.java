package com.homework.goit.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Utilita {
    private static final Logger LOG = LogManager.getFormatterLogger(Utilita.class);
    private static View view = new Console();

    public static int validateNumber(String value) {
        LOG.debug(String.format("validateNumber: value='%s'", value));
        int res = 0;
        try {
            res = Integer.parseInt(value);
        } catch (RuntimeException e) {
            LOG.error(String.format("validateNumber:error with value '%s'", value), e);
            view.write("Input not a number!");
        }
        return res;
    }

    public static String validate(String value) {
        LOG.debug(String.format("validate: value='%s'", value));
        while (value.strip().isEmpty()) {
            view.write("Field can't be empty");
            value = view.read();
        }
        return value;
    }

    public static LocalDate validateDate(String value) {
        LOG.debug(String.format("validateDate: value='%s'", value));
        if (value.strip().isBlank()) {
            return null;
        }
//        LocalDate date;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d");
        try {
            return LocalDate.parse(value.strip(), formatter);
        } catch (Exception e) {
            LOG.error(String.format("validateDate:error with value '%s'", value), e);
            view.write("Wrong date format!");
        }
        return LocalDate.EPOCH;
    }
}
