package com.homework.goit.common.command;

import com.homework.goit.common.Command;
import com.homework.goit.common.View;

public class Help implements Command {
    private View view;

    public Help(View view) {
        this.view = view;
    }

    @Override
    public String command() {
        return "help";
    }

    @Override
    public void execute() {
        view.write("------------------------------------------------------------------------------");
        view.write("-------------------------  Commands list  ------------------------------------");
        view.write("------------------------------------------------------------------------------");
        view.write("-   Command:                Description:");
        view.write("-   help                    - shows all available commands");
        view.write("-   create_company          - create new company");
        view.write("-   update_company          - update existing company by id");
        view.write("-   get_company_by_id       - show company by id ");
        view.write("-   get_all_companies       - show all companies ");
        view.write("-   delete_company          - delete company be id ");
        view.write("");
        view.write("-   create_customer         - create new customer");
        view.write("-   update_customer         - update existing customer by id");
        view.write("-   get_customer_by_id      - show customer by id");
        view.write("-   get_all_customers       - show all customers");
        view.write("-   delete_customer         - delete customer be id");
        view.write("");
        view.write("-   create_project          - create new project");
        view.write("-   update_project          - update existing project by id");
        view.write("-   get_project_by_id       - show project by id");
        view.write("-   get_all_projects        - show all projects");
        view.write("-   delete_project          - delete project be id");
        view.write("");
        view.write("-   create_developer        - create new developer");
        view.write("-   update_developer        - update existing developer by id");
        view.write("-   get_developer_by_id     - show developers by id");
        view.write("-   get_all_developers      - show all developers");
        view.write("-   delete_developer        - delete developer be id");
        view.write("");
        view.write("-   total_salary_in_project - show total developers salary in project with id");
        view.write("-   show_projects_info      - show base projects information");
        view.write("");
        view.write("-   show_java_developers    - show list of all Java developers");
        view.write("-   show_middle_developers  - show list of all middle developers");
        view.write("-   show_developers_in_project - show list of developers in project with id");
        view.write("");
        view.write("-   exit                     - stop application");
        view.write("------------------------------------------------------------------------------");
        view.write(" ");
    }
}
