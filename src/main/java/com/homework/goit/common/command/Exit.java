package com.homework.goit.common.command;

import com.homework.goit.common.Command;
import com.homework.goit.common.View;

public class Exit implements Command {
    private View view;

    public Exit(View view) {
        this.view = view;
    }

    @Override
    public String command() {
        return "exit";
    }

    @Override
    public void execute() {
        view.write("Bye!");
        System.exit(0);
    }
}
