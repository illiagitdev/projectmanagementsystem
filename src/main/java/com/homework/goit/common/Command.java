package com.homework.goit.common;

public interface Command {
    String command();

    void execute();

    default boolean canExecute(String command){
        return command.trim().equals(command());
    }
}

