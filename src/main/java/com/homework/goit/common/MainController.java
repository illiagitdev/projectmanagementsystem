package com.homework.goit.common;

import com.homework.goit.common.command.Exit;
import com.homework.goit.common.command.Help;
import com.homework.goit.company.command.*;
import com.homework.goit.customer.command.*;
import com.homework.goit.developer.command.*;
import com.homework.goit.project.command.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

public class MainController {
    private static final Logger LOG = LogManager.getFormatterLogger(MainController.class);
    private View view;
    private List<Command> commands;

    public MainController(View view) {
        this.view = view;
        commands = Arrays.asList(
                new Help(view),
                new CreateCompany(view),
                new UpdateCompany(view),
                new GetCompanyById(view),
                new GetAllCompanies(view),
                new DeleteCompany(view),

                new CreateCustomer(view),
                new UpdateCustomer(view),
                new GetCustomerById(view),
                new GetAllCustomers(view),
                new DeleteCustomer(view),

                new CreateProjects(view),
                new UpdateProjects(view),
                new GetProjectById(view),
                new GetAllProjects(view),
                new DeleteProject(view),

                new CreateDeveloper(view),
                new UpdateDeveloper(view),
                new GetDeveloperById(view),
                new GetAllDevelopers(view),
                new DeleteDeveloper(view),

                new TotalSalaryInProjects(view),
                new ShowProjectsInfo(view),

                new ShowJavaDevelopers(view),
                new ShowMiddleDevelopers(view),
                new ShowDevelopersInProject(view),

                new Exit(view)
        );
    }

    public void process(){
        view.write("Welcome to Project Management System!");
        while (true){
            view.write("Enter commands ('help' to see list of commands)!)");
            String actions = view.read();
            doCommand(actions);
        }
    }

    private void doCommand(String actions) {
        LOG.debug(String.format("doCommand:action=%s", actions));
        for (Command command: commands) {
            if (command.canExecute(actions)){
                command.execute();
                break;
            }
        }
    }
}
